//Medidas Circulo: Circulo
import java.util.Scanner;

public class Circulo
{
   private int raio;
   public Circulo(int raio) {
      this.raio = raio;
   }
   public int getRaio() {
      return raio;
   }
   public double getPerimetro() {
      return 2*raio*Math.PI;
   }
   public double getArea() {
      return Math.PI * raio * raio;
   }
   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      System.out.println("Digite o valor do Raio:");
      int raio = Integer.parseInt(in.nextLine()); //Use apenas nextLine() para ler do console
      Circulo circulo = new Circulo(raio);
      System.out.printf("Comprimento da circunferência: %.2f%n", circulo.getPerimetro());
      System.out.printf("Área da circunferência: %.2f%n", circulo.getArea());
   }
}